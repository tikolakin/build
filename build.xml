<?xml version="1.0"?>

<!-- File: build.xml -->

<project name="phing-drupal-install" default="build">
  
  <target name="build" depends="load-properties, setup-dirs, make, site-install, dump, simpletest"/>
  <target name="test" depends="load-properties, clean, restore, simpletest, selenium"/>


  
  <!-- ### Initialization -->
  <target name="init"
          depends="load-properties, setup-dirs, make, site-install, dump"
          unless="project.initialized">
    <!-- Set property to prevent target from being executed multiple times -->
    <property name="project.initialized" value="true"/>
  </target>
  
  <!-- ### Load properties
  Loads a set of project specific properties from a `.properties` file.
  These properties contain information regarding the individual project and/or
  environment such as which version of Drupal you are using, how to create a
  database and the names of your custom modules. -->

  <target name="load-properties">
    <php function="dirname" returnProperty="phing.dir">
      <param value="${phing.file}"/>
    </php>

    <property name="project.base.dir" value="${phing.dir}" />

    <!-- Use condition instead of unless property as we cannot unset properties in Phing -->
    <if>
      <or>
        <not><istrue value="${project.properties.loaded}" /></not>
        <not><isset property="project.properties.loaded" /></not>
      </or>
      <then>
          <if>
          <!-- If `build.properties` exists then assume we have a
               project root directory -->
          <available file="${project.base.dir}/build.properties"/>
          <then>
            <resolvepath propertyName="project.base.dir"
                         file="${project.base.dir}/"/>
          </then>
        </if>
        <!-- Allow override using `build.properties` in build file
             directory -->
        <available file="${phing.dir}/build.properties"
                   property="project.properties.file"
                   value="${phing.dir}/build.properties" />
        <!-- Load the overriding properties. -->
        <property file="${project.properties.file}" override="true" />
        <!-- Set property to prevent unnecessary additional invocations of this target -->
        <property name="project.properties.loaded" value="true" />
      </then>
    </if>
  </target>


  <!-- Setup directories -->
  <target name="setup-dirs" depends="load-properties">
    <property name="project.build.dir" value="${project.base.dir}/build"/>
    <property name="project.tools.dir" value="${project.base.dir}/build/tool"/>
    <property name="project.testreports.dir" value="${project.build.dir}/testreports"/>
    <property name="project.drupal.dir" value="${project.base.dir}/drupal/"/>
    <property name="project.dump.dir" value="${project.base.dir}/dump/"/>
    <property name="project.make.dir" value="${project.base.dir}/make/"/>
    <property name="project.repo.dir" value="${project.base.dir}/repo/"/>
  </target>

  <!-- ### Clean working environment  -->
  <target name="clean"
          description="Clean up and create artifact directories"
          depends="setup-dirs"
          unless="project.cleaned">
    <!-- Delete any existing artifacts from a previous build.
         Do not delete builddir. It may contain the build file! -->
    <delete dir="${project.tools.dir}"/>
    <delete dir="${project.testreports.dir}"/>
    
    <!-- Recreate directories for artifacts -->
    <mkdir dir="${project.tools.dir}"/>
    <mkdir dir="${project.testreports.dir}"/>

    <!-- Set property to prevent target from being executed multiple times -->
    <property name="project.cleaned" value="true"/>
  </target>

  
  <!-- ### Drush make -->
  <target name="make" depends="setup-phing-drush">
    <if>
    	<not><available file="${project.make.dir}"/></not>
    	<then>
		    <!-- Clone the project -->
		    <phingcall target="setup-git-repo">
		      <property name="repo.revision" 
		        value="${drupal.make.repository.revision}" />
		      <property name="repo.url"
		        value="${drupal.make.repository.url}" />
		      <property name="repo.dir"
		        value="${project.make.dir}"/>
		    </phingcall>
		</then>
	</if>
		  <!-- Delete any prexisting builds -->
		    <exec command="sudo rm -Rif ${project.drupal.dir}"/>
	    <!-- Run drush from the project.make directory -->
	    <property name="drush.root" value="${project.make.dir}"/>
	    <!-- Make from repo dir to drupal dir -->
	    <drush command="make">
	    	<param>${project.make.dir}${drupal.make.file}</param>
	    	<param>${project.drupal.dir}</param>
	    </drush>
  </target>

  <!-- ### Install a Drupal site
  This initializes a Drupal site using a installation profile.
  Configuration of which installation profile and database to use in done in
  `build.properties`. -->
  <target name="site-install"
          depends="setup-phing-drush"
          unless="project.installed">
  <!-- Set proprorties for cloning repo --> 
        <echo>Delete any preexisting drupal dir: ${project.drupal.dir}</echo>
        <!-- Delete any prexisting builds -->
        <exec command="sudo rm -Rif ${project.drupal.dir}"/>
        <echo>Clone repo and switch to ${repo.branch} branch</echo>
        <phingcall target="setup-git-repo">
          <property name="repo.dir" value="${project.drupal.dir}"/>
          <property name="repo.url" value="${repo.url}"/>
          <property name="repo.revision" value="${repo.branch}"/>
        </phingcall>
    <!-- Run drush from the project.make directory -->
    <property name="drush.root" value="${project.drupal.dir}"/>

    <drush command="site-install" assume="yes">
      <option name="db-url">${drupal.db.url}</option>
      <param>${drupal.profile}</param>
      <param>${drupal.currency}</param>
      <option name="account-name">${account.name}</option>
      <option name="account-pass">${account.pass}</option>
      <option name="site-name">CommerceBox</option>
      <!-- <option name="account-mail">${account.mail}</option>
      <option name="site-mail">${site.mail}</option> -->
    </drush>
    <echo>Fix dirs permission</echo>
    <phingcall target="fixPermisions"/>

    <!-- Set property to prevent target from being executed multiple times -->
    <property name="project.installed" value="true"/>
  </target>


 <!-- ### Run simpletests

Execution of this target can be skipped by setting the
 `project.simpletest.skip` property from the command line or in other targets.
-->
  <target name="simpletest"
          description="Run all functional tests"
          depends="setup-phing-drush"
          unless="project.simpletest.skip">
    <!-- Enable simpletest module. If using Drupal 6 the module will be
         downloaded as well. -->
    <phingcall target="enable-module">
      <property name="module" value="simpletest_clone"/>
    </phingcall>

    <if>
      <isset property="drupal.uri" />
      <then>
    <!-- Run the tests and generate JUnit XML reports. -->
        <drush command="test-run" uri="${drupal.uri}" root="${project.drupal.dir}" haltonerror="false">
            <option name="xml">${project.testreports.dir}</option>
            <param>CommerceBox</param>
         </drush>
      </then>
      <else>
        <echo msg="You must set the drupal.uri property to get simpletests working." />
      </else>
    </if>
    
  </target>
  
  <!-- Prepare for selenium test -->
  <target name="selenium" depends="setup-phing-drush, restore">
    <!-- load backup before run selenium  tests -->
  </target>

  <!-- ### Help targets -->

  <!-- ### Setup Phing Drush integration -->
  <target name="setup-phing-drush"
          depends="setup-dirs" >
    <!-- Clone the project -->
    <phingcall target="setup-git-repo">
      <property name="repo.revision"
                value="${phing.drush.repository.revision}" />
      <property name="repo.url"
                value="${phing.drush.repository.url}" />
      <property name="repo.dir"
                value="${project.tools.dir}/phing-drush"/>
    </phingcall>

    <!-- Register as custom Phing task -->
    <taskdef name="drush" classname="DrushTask"
             classpath="${project.tools.dir}/phing-drush" />

    <!-- Run drush from the project Drupal directory -->
    <property name="drush.root" value="${project.drupal.dir}" override="true"/>
  </target>


  <!-- ### Download and enable a project/module -->
  <target name="enable-module"
          depends="setup-phing-drush">
    <!-- If project is not set then we assume that the module name is also
         the project name. -->
    <property name="project" value="${module}" override="no"/>

    <!-- If the module is not already available then download it -->
    <drush command="pm-list" returnProperty="modules.available"/>
    <php function="strpos" returnProperty="module.available">
      <param>${modules.available}</param>
      <param>${module}</param>
    </php>
    <if>
      <not><istrue value="${module.available}"/></not>
      <then>
        <!-- Download specific version if specified -->
        <condition property="download" value="${project}-${project.version}">
          <isset property="project.version"/>
        </condition>
        <property name="download" value="${project}" override="false"/>

        <drush command="pm-download" assume="yes">
          <param>${download}</param>
        </drush>
      </then>
    </if>

    <!-- Enable the module -->
    <drush command="pm-enable" assume="yes">
      <param>${module}</param>
    </drush>
  </target>

   <!-- ### Clone a git repository -->
  <target name="setup-git-repo">
    <property name="repo.revision" value="${repo.branch}" override="false"/>
    <!-- Only clone if repository does not exist already else Pull-->
    <if>
      <not><available file="${repo.dir}"/></not>
      <then>
        <!-- Set revision to HEAD if not already defined -->

        <echo>Cloning ${repo.revision} ${repo.url} into ${repo.dir}</echo>
        <!-- The [`gitclone` task](http://www.phing.info/docs/guide/stable/chapters/appendixes/AppendixC-OptionalTasks.html#GitCloneTask)
             does not seem to work. Use exec instead. -->
        <exec command="git clone ${repo.url} ${repo.dir}" />
        <echo>Switch to ${repo.revision} branch</echo>
        <exec command="git checkout ${repo.revision}" dir="${repo.dir}"/>
      </then>
    </if>
  </target>

  <!-- Fix permissions for the default site directory and settings. -->
  <target name="fixPermisions" depends="setup-dirs">
    <exec command="chmod -R 777 ${project.drupal.dir}/sites/all"/>
    <exec command="chmod -R 777 ${project.drupal.dir}/sites/default/files"/>
    <exec command="chmod 755 ${project.drupal.dir}/sites/default"/>
    <exec command="chmod 755 ${project.drupal.dir}/sites/default/default.settings.php"/>
  </target>

  <!-- Dump -->
  <target name="dump" depends="setup-phing-drush">
    <property name="archive" value="dump"/>
    <echo>Make site dump to ${project.dump.dir}${archive}.tar</echo>
    <drush command='archive-dump'>
      <option name="destination">${project.dump.dir}${archive}.tar</option>
      <option name="overwrite"></option>
    </drush>
  </target>
  
  <!-- Restore dump -->
  <target name="restore" depends="setup-phing-drush">
    <exec command="sudo rm -Rif ${project.drupal.dir}"/>
    <property name="archive" value="dump"/>
    <!-- load backup before run selenium  tests -->
    <drush command="archive-restore">
      <param>${project.dump.dir}${archive}.tar</param>
      <option name="overwrite"></option>
    </drush>
    <!-- Set dir permisions -->
    <phingcall target="fixPermisions"/>
  </target>
</project>