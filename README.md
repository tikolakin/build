# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Phing Drupal build
* 1.0

### How do I get set up? ###

> For running phing first you need install throw pear:

    sudo apt-get install php-pear
    sudo pear channel-discover pear.phing.info
    sudo pear install [--alldeps] phing/phing
>
> Then configure you localhost to easy access you site

       <Virtualhost *:80>
		VirtualDocumentRoot "/var/www/test/%1/drupal"
		ServerName test
		ServerAlias *.test
		UseCanonicalName Off
		LogFormat "%V %h %l %u %t \"%r\" %s %b" vcommon
		ErrorLog "/var/www/vhosts-error_log"
		<Directory "/var/www/test/*">
		Options Indexes FollowSymLinks MultiViews
		AllowOverride All
		Order allow,deny
		Allow from all 
		RewriteEngine on
		RewriteBase /drupal
		RewriteCond %{REQUEST_FILENAME} !-f
		RewriteCond %{REQUEST_FILENAME} !-d
		RewriteCond %{REQUEST_URI} ^/drupal/
		RewriteRule ^([^/]+)/?$ index.php?q=$1 [L,QSA] 
		</Directory>
       </Virtualhost>

> and create in you /var/www folder with "test" name where you should put all you sites
>
> Copy this repo into /var/www/test with folder name what you prefer, i.e. themejournal

> Go to /var/www/test/site and run

             phing -f build.xml site-install
> All configuration for you installation, i.e. database name, drupal admin account, repo and branches you can specify in buil.proporties

> After installation open browser and navigate to http://themejournal.test

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact1.0